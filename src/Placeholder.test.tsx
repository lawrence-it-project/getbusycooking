import React from 'react';
import {render, screen} from "@testing-library/react";

describe('rendering a react component', () => {
    test('displays the component', () => {
        render(<h1>Test</h1>)

        expect(screen.getByText('Test')).toBeVisible();
    });
});

describe('using typescript', () => {
    test('does not crash with a syntax error', () => {
        interface TestDataInterface {
            test: boolean;
        }

        const testData: TestDataInterface = {test: true};

        expect(testData).toHaveProperty('test', true);
    });
});
